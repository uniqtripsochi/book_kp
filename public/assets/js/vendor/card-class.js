class Card {
  constructor() {
    (this.$cards = document.querySelectorAll(".js-card")),
      this.$dotsWrap,
      this.height,
      this.setup();
  }
  setup = () => {
    this.$cards.forEach((t) => {
      (this.height = t.offsetHeight),
        (this.$dotsWrap = t.querySelectorAll(".js-dots")),
        this.fillDots(this.$dotsWrap);
    });
  };
  fillDots = (t) => {
    const e = Math.ceil(this.height / 16);
    t.forEach((s) => {
      s.insertAdjacentHTML(
        "afterbegin",
        '<div class="card-dots__dot-big"></div>'
      );
      for (let t = 0; t < e; t++)
        s.insertAdjacentHTML("beforeend", '<div class="card-dots__dot"></div>');
      s.insertAdjacentHTML(
        "beforeend",
        '<div class="card-dots__dot-big"></div>'
      );
    });
  };
}
